<?php

ini_set('max_execution_time', 300);
header('Content-Type: text/html; charset=utf-8');
require_once 'app/admin/database.php';

$all_urls = array(

    //Nezařazené
    'https://echo24.cz/rss/s/homepage',
    'http://www.ceskatelevize.cz/ct24/rss/hlavni-zpravy',
    'http://www.ceskatelevize.cz/sport/rss/hlavni-zpravy/',
    'http://www.ceskatelevize.cz/ct24/rss/morava',
    'http://www.ceskatelevize.cz/ct24/rss/slezsko',
    'https://www.euro.cz/rss',
    'https://www.euro.cz/byznys/rss',
    'https://www.euro.cz/miliardari/rss',
    'https://www.euro.cz/politika/rss',
    'https://www.euro.cz/udalosti/rss',
    'https://www.euro.cz/manazerske-vzdelavani/rss',
    'https://www.euro.cz/praha/rss',
    'https://www.euro.cz/light/rss',
    'https://www.euro.cz/blogy/rss',
    'https://www.euro.cz/tv/rss',
    'https://www.euro.cz/kalkulacky/rss',
    'https://www.euro.cz/energetika/rss',
    'https://www.euro.cz/spolecenska-odpovednost-firem/rss',
    'https://www.euro.cz/prezidentske-volby-2018/rss',
    'http://hlidacipes.org/feed/',
    'https://www.mediaguru.cz/rss/',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1168',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1169',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1170',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1171',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1172',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1174',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1173',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1175',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1176',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1177',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1178',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1179',
    'https://www.mediaguru.cz/rss/?type=kategorie&id=1180',
    'http://www.mediar.cz/feed/',
    'http://servis.metro.cz/rss.aspx',
    'http://www.peak.cz/feed/',
    'http://tv.nova.cz/lbin/micro_rss.php',
    'https://www.aktualne.cz/mrss/',
    'https://www.aktualne.cz/rss/',
    'https://zpravy.aktualne.cz/rss/?_ga=2.26636733.1052239632.1512086699-1419645502.1506286277',
    'https://zpravy.aktualne.cz/rss/domaci/?_ga=2.26636733.1052239632.1512086699-1419645502.1506286277',
    'https://zpravy.aktualne.cz/rss/zahranici/?_ga=2.29668671.1052239632.1512086699-1419645502.1506286277',
    'https://zpravy.aktualne.cz/rss/regiony/?_ga=2.29668671.1052239632.1512086699-1419645502.1506286277',
    'https://zpravy.aktualne.cz/rss/ekonomika/?_ga=2.29668671.1052239632.1512086699-1419645502.1506286277',
    'https://zpravy.aktualne.cz/rss/finance/?_ga=2.29668671.1052239632.1512086699-1419645502.1506286277',
    'https://zpravy.aktualne.cz/rss/pocasi/?_ga=2.29668671.1052239632.1512086699-1419645502.1506286277',
    'https://sport.aktualne.cz/rss/?_ga=2.201758225.1052239632.1512086699-1419645502.1506286277',
    'https://sport.aktualne.cz/rss/hokej/?_ga=2.201758225.1052239632.1512086699-1419645502.1506286277',
    'https://sport.aktualne.cz/rss/fotbal/?_ga=2.201758225.1052239632.1512086699-1419645502.1506286277',
    'https://sport.aktualne.cz/rss/motorismus/?_ga=2.154448056.1052239632.1512086699-1419645502.1506286277',
    'https://sport.aktualne.cz/rss/ostatni-sporty/?_ga=2.155146808.1052239632.1512086699-1419645502.1506286277',
    'https://nazory.aktualne.cz/rss/?_ga=2.155146808.1052239632.1512086699-1419645502.1506286277',
    'http://blog.aktualne.cz/export-all.php?_ga=2.155146808.1052239632.1512086699-1419645502.1506286277',
    'https://magazin.aktualne.cz/rss?_ga=2.155146808.1052239632.1512086699-1419645502.1506286277',
    'https://video.aktualne.cz/rss/?_ga=2.155146808.1052239632.1512086699-1419645502.1506286277',
    'https://video.aktualne.cz/rss/dvtv/?_ga=2.155146808.1052239632.1512086699-1419645502.1506286277',
    'https://www.sport.cz/rss2',
    'https://www.zive.cz/rss/sc-47/',
    'https://doupe.zive.cz/rss',
    'https://www.pcrevue.sk/site/rssfeed/type/Magazin/page/1',
    'http://www.grafika.cz/rss/rss-aktuality-cz-grafika.cz.xml',
    'https://cdr.cz/cdrrss.php',
    'https://www.root.cz/rss/clanky/',
    'https://www.root.cz/rss/zpravicky/',
    'https://forum.root.cz/index.php?action=.xml;type=rss2;limit=30;sa=news',
    'https://www.root.cz/rss/knihy/',
    'https://blog.root.cz/rss/',
    'https://www.root.cz/rss/autori/petr-krcmar/',
    'https://www.root.cz/rss/clanky/r/webove-prohlizece/',
    'http://www.reflex.cz/rss',
    'http://www.abcgames.sk/data/rss.xml',
    'https://www.playtvak.cz/rss.aspx',
    'http://www.ceskenoviny.cz/sluzby/rss/zpravy.php',
    'http://www.ceskenoviny.cz/sluzby/rss/cr.php',
    'http://www.ceskenoviny.cz/sluzby/rss/svet.php',
    'http://www.ceskenoviny.cz/sluzby/rss/ekonomika.php',
    'http://www.ceskenoviny.cz/sluzby/rss/kultura.php',
    'http://www.ceskenoviny.cz/sluzby/rss/magazin.php',
    'http://www.ceskenoviny.cz/sluzby/rss/sport.php',
    'http://www.ceskenoviny.cz/sluzby/rss/fotbal.php',
    'http://www.ceskenoviny.cz/sluzby/rss/hokej.php',
    'http://www.ceskenoviny.cz/sluzby/rss/tenis.php',
    'http://www.denik.cz/rss/z_domova.html',
    'http://www.denik.cz/rss/ekonomika.html',
    'http://www.denik.cz/rss/ze_sveta.html',
    'http://www.denik.cz/rss/sport.html',
    'http://www.denik.cz/rss/kultura.html',
    'http://www.denik.cz/rss/bydleni.html',
    'http://www.denik.cz/rss/cestovani.html',
    'http://www.denik.cz/rss/auto.html',
    'http://www.denik.cz/rss/zdravi.html',
    'http://www.denik.cz/rss/komentare.html',
    'http://ihned.cz/?m=rss',
    'http://ihned.cz/?m=tagsRSS&overview[tag_id]=265703',
    'http://byznys.ihned.cz/?p=02R000_rss',
    'http://domaci.ihned.cz/?m=rss',
    'http://zahranicni.ihned.cz/?m=rss',
    'http://nazory.ihned.cz/?m=rss',
    'http://blog.ihned.cz/?m=rss',
    'http://life.ihned.cz/?m=rss',
    'http://tech.ihned.cz/?m=rss',
    'http://art.ihned.cz/?m=rss',
    'http://auto.ihned.cz/?m=rss',
    'http://archiv.ihned.cz/?m=rss',
    'http://ekonom.ihned.cz/?m=rss',
    'http://mam.ihned.cz/?m=rss',
    'http://logistika.ihned.cz/?m=rss',
    'http://modernirizeni.ihned.cz/?m=rss',
    'http://pravniradce.ihned.cz/?m=rss',
    'http://kariera.ihned.cz/?m=rss',
    'http://servis.idnes.cz/rss.aspx',
    'http://servis.idnes.cz/rss.aspx?c=komiksy',
    'http://servis.idnes.cz/rss.aspx?c=iglobe',
    'http://servis.idnes.cz/rss.aspx?c=reality',
    'http://servis.idnes.cz/rss.aspx?c=autokat',
    'http://servis.idnes.cz/rss.aspx?c=hobby',
    'http://servis.idnes.cz/rss.aspx?c=technet',
    'http://servis.idnes.cz/rss.aspx?c=mobil',
    'http://servis.idnes.cz/rss.aspx?c=cas',
    'http://servis.idnes.cz/rss.aspx?c=rungo',
    'http://servis.idnes.cz/rss.aspx?c=xman',
    'http://servis.idnes.cz/rss.aspx?c=ona',
    'http://servis.idnes.cz/rss.aspx?c=fincentrum',
    'http://servis.idnes.cz/rss.aspx?c=zlin',
    'http://servis.idnes.cz/rss.aspx?c=usti',
    'http://servis.idnes.cz/rss.aspx?c=usti',
    'http://servis.idnes.cz/rss.aspx?c=pardubice',
    'http://servis.idnes.cz/rss.aspx?c=ostrava',
    'http://servis.idnes.cz/rss.aspx?c=olomouc',
    'http://servis.idnes.cz/rss.aspx?c=liberec',
    'http://servis.idnes.cz/rss.aspx?c=vary',
    'http://servis.idnes.cz/rss.aspx?c=jihlava',
    'http://servis.idnes.cz/rss.aspx?c=hradec',
    'http://servis.idnes.cz/rss.aspx?c=budejovice',
    'http://servis.idnes.cz/rss.aspx?c=brnoh',
    'http://servis.idnes.cz/rss.aspx?c=prahah',
    'http://servis.idnes.cz/rss.aspx?r=kavarna',
    'http://servis.idnes.cz/rss.aspx?c=kultura',
    'http://servis.idnes.cz/rss.aspx?c=ekonomikah',
    'http://servis.idnes.cz/rss.aspx?c=basket',
    'http://servis.idnes.cz/rss.aspx?r=volejbal',
    'http://servis.idnes.cz/rss.aspx?r=tenis',
    'http://servis.idnes.cz/rss.aspx?c=hokejh',
    'http://servis.idnes.cz/rss.aspx?c=fotbalh',
    'http://servis.idnes.cz/rss.aspx?c=sport',
    'http://servis.idnes.cz/rss.aspx?c=zpravodaj',
    'https://www.novinky.cz/rss2/vase-zpravy/',
    'http://www.tyden.cz/rss/rss.php?all',
    'http://www.blesk.cz/rss',
    'http://www.blesk.cz/rss/titul.xml',
    'http://www.sportovninoviny.cz/sluzby/rss/hokej.php',
    'http://www.volejbal.cz/rss.php',

    //Dezinformace
    'http://ac24.cz/?format=feed&type=rss'



);

foreach ($all_urls as $url) {

    try {

        //Kontrola správnosti xml
        $rss = simplexml_load_file($url);
        echo $url;

    } catch (Exception $e) {

        continue;

    }

    //Už v každé xml samostatně, pro každý item (článek)
    foreach ($rss->channel->item as $clanek) {

        // TADY prostě str_replace všechny zasraný & za asi and
        $removedUpmTitle = str_replace("&", "&amp;", $clanek->title);
        $removedUpmDescription = str_replace("&", "&amp;", $clanek->description);

        $gu_id = $clanek->guid;
        $title = $removedUpmTitle;
        $link = $clanek->link;

        $rss_desc = $removedUpmDescription;

        //Smazaní cData obsahu
        $startPoint = '<ul>';
        $endPoint = '</ul>';
        $description = preg_replace('#('.preg_quote($startPoint).')(.*)('.preg_quote($endPoint).')#si', '', $rss_desc);

        //XML date to SQL date
        $rss_pd = $clanek->pubDate;
        $publish_date = strftime("%Y-%m-%d %H:%M:%S", strtotime($rss_pd));

        $category = $clanek->category;
        $server = $rss->channel->title;

        //Nastavení prázdných proměnných pro vložení a kontrola, jestli nechybí guid a description.
        //Pokud chybí, uloží se čas a adresa, kde chyba nastala
        if (isset($clanek->guid)) {

            $gu_id = $clanek->guid;

        } else {

            echo ' !!!! <p style="color:red;">GU ID nesmí chybět!</p> !!!! ';

            //Uložení záznamu o chybeném xml
            $dotaz_chyba = $databaseConnection->prepare("INSERT INTO log_url (url) VALUES(?)");
            $vlozeni_chyby = $dotaz_chyba->execute(array($url));

        }

        if (isset($clanek->title)) {

            $title = $clanek->title;
        } else {

            $title = '';

        }

        if (isset($clanek->link)) {

            $link = $clanek->link;

        } else {

            $link = '';

        }

        $find   = "    ";

        if( strpos( $description, $find ) == false ) {

            //Smazaní cData obsahu
            $startPoint = '<ul>';
            $endPoint = '</ul>';
            $description = preg_replace('#('.preg_quote($startPoint).')(.*)('.preg_quote($endPoint).')#si', '', $rss_desc);


        } else {

            echo ' !!!! <p style="color:blue;">DESCRIPTION nesmí chybět!</p> !!!! ';

            //Uložení záznamu o chybném xml
            $dotaz_chyba2 = $databaseConnection->prepare("INSERT INTO log_url (url) VALUES(?)");
            $vlozeni_chyby2 = $dotaz_chyba2->execute(array($url));
        }

        if (isset($clanek->pubDate)) {

            $publish_date = strftime("%Y-%m-%d %H:%M:%S", strtotime($rss_pd));;

        } else {

            $publish_date = '';

        }

        if (isset($clanek->category)) {

            $category = $clanek->category;

        } else {

            $category = '';

        }

        if (isset($rss->channel->title)) {

            $server = $rss->channel->title;

        } else {

            $server = '';
        }

        //Kontrola, jestli už globální id (článek) není v DB

        $dotaz_vybrat = $databaseConnection->prepare("SELECT * FROM articles WHERE gu_id = :gu_id");
        $dotaz_vybrat->bindParam(':gu_id', $gu_id);
        $dotaz_vybrat->execute();
        $vysledky = $dotaz_vybrat->fetch();

        //Pokud není, uloží se
        if ($vysledky == 0) {

            $dotaz_vlozit = $databaseConnection->prepare("INSERT INTO articles ( gu_id, title, link, description, publish_date, category, server ) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $dotaz_vlozit->execute([$gu_id, $title, $link, $description, $publish_date, $category, $server]);

            echo '<p style="color:green;">Uloženo!</p>';

        } else {

            echo '<p style="color:red;">Už to tu je!</p>';

        };

    };

};