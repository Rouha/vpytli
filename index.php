<!DOCTYPE html>
<html>
    <title>vPYTLI.cz</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="styles/dist/compiled_styles.min.css">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>

    <script src="js/jquery.min.js"></script>
    <script src="js/type.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({
                showOtherMonths: true,
                selectOtherMonths: true
            });
        } );
    </script>


<body>


    <div id="Page">

        <div id="Header" class="noselect">

            <a href="#" title="vpytli.cz">

                <div class="logo">

                    <span>vpytli.cz</span>


                </div>

            </a>

            <div class="separator"></div>

            <div class="text">

                <span class="firstRotate"> </span></br>
                <span class="secondRotate"> </span>

            </div>


            <div class="info">

                <span><i class="fa fa-times fa-2x" aria-hidden="true" title="Zrušit filtr"></i></span>
                <span><i class="fa fa-question fa-2x" aria-hidden="true" title="Informace o stránce"></i></span>
                <span class="textovka"><i class="fa fa-minus fa-2x fontMinus" aria-hidden="true" title="Velikost textu"></i></span>
                <span class="textovka"><i class="fa fa-plus fa-2x fontPlus" aria-hidden="true" title="Velikost textu"></i></span>

            </div>



        </div>

        <div class="clearfix"></div>

        <div id="Filter">

            <div class="row">
                <div class="col-sm-4">

                    <span class="filterTitle">Vyhledávání</span>

                    <div class="input-group mb-2 mr-sm-2 mb-sm-0 searchBoxFilter">
                        <input type="text" class="form-control searchid" id="inlineFormInputGroup" placeholder="slovo, slovní spojení">
                            <div class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></div>
                    </div>

                    <span class="filterTitle">Časové rozmezí</span>

                    <form class="dateRadioFilter">

                        <span class="button-checkbox">
                            <button type="button" class="btn radioBtn" data-color="primary">1 hodina</button>
                            <input type="radio" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn radioBtn" data-color="primary">12 hodin</button>
                            <input type="radio" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn radioBtn" data-color="primary">24 hodin</button>
                            <input type="radio" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn radioBtn" data-color="primary">1 týden</button>
                            <input type="radio" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn radioBtn" data-color="primary">1 měsíc</button>
                            <input type="radio" class="hidden radioBtn" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn radioBtn" data-color="primary">1 rok</button>
                            <input type="radio" class="hidden" />
                        </span>

                        <span class="button-checkbox ownDate">
                            <button type="button" class="btn radioBtnOwn" data-color="primary">vlastní</button>
                            <input type="radio" class="hidden" />
                        </span>

                    </form>



                </div>

                <div class="col-sm-8">

                    <form class="categoryCheckbox">

                        <span class="filterTitle">Kategorie</span>

                        <span class="button-checkbox">
                            <button type="button" class="btn politics" data-color="primary">politika</button>
                            <input type="checkbox" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn sport" data-color="primary">sport</button>
                            <input type="checkbox" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn culture" data-color="primary">kultura</button>
                            <input type="checkbox" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn economy" data-color="primary">ekonomika</button>
                            <input type="checkbox" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn abroad" data-color="primary">zahraničí</button>
                            <input type="checkbox" class="hidden" />
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn technology" data-color="primary">technika</button>
                            <input type="checkbox" class="hidden" />
                        </span>


                    </form>



                </div>

            </div>

        </div>

        <div class="filterEnd clearfix"></div>

        <div id="List">


        </div>

        <div id="Footer">

            <div id="InnerFooter">

                <span class="copyright">Jan Rouha © 2017</span>

                <span class="arrowUp noClickArrow"><i class="fa fa-arrow-up fa-2x" aria-hidden="true" title="Zpět nahoru"></i></span>

                <div class="paginator">
                    <span>

                    </span>
                </div>


            </div>

        </div>





    </div>





</body>
</html>