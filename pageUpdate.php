<?php

require_once "app/admin/database.php";

$text = $_POST['text'];
if (isset($_POST['page'])) $page = $_POST['page'];
else $page = 1;

$category = $_POST['category'];

try {

    $subQuery = "";

    if (isset($category[0])){

        for ($i = 0; $i < sizeof($category); $i++) {
            if ($i == 0)
                $subQuery .= "AND category LIKE '%".$category[$i]."%'";
            else
                $subQuery .= " OR category LIKE '%$category[$i]%'";
        }

    }

    // Find out how many items are in the table
    $total = $databaseConnection->query("SELECT COUNT(*) FROM articles where title LIKE '%$text%' OR description LIKE '%$text%' $subQuery")->fetchColumn();

    // How many items to list per page
    $limit = 20;

    // How many pages will there be
    $pages = ceil($total / $limit);

    // Calculate the offset for the query
    $offset = ($page - 1)  * $limit;

    // Some information to display to the user
    $start = $offset + 1;
    $end = min(($offset + $limit), $total);

    // The "back" link
    $prevlink = ($page > 1) ? '<a href="?page=1" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) .
        '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';

    // The "forward" link
    $nextlink = ($page < $pages) ? '<a href="?page=' . ($page + 1) . '" title="Next page" id="next">&rsaquo;</a> <a href="?page=' . $pages .
        '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';

    echo '<div id="paging"><p>', $prevlink, ' Strana ', $page, ' z ', $pages, ' stran, zobrazeno ', $start, '-', $end, ' z ', $total, ' výsledků ', $nextlink, ' </p></div>';

} catch (Exception $e) {
    echo '<p>', $e->getMessage(), '</p>';
}

