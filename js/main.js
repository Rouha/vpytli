$( document ).ready(function() {

    first();
    second();
    test();
    filter();

    var page=1;

    function first(){

        var items = ["důvěřuj, ale prověřuj", "psané slovo na jednom místě"];
        var item = items[Math.floor(Math.random()*items.length)];

        $('.firstRotate').typed({
            strings: [item, ""], //Druhý parametr pro opačné slovo
            // Optionally use an HTML element to grab strings from (must wrap each string in a <p>)
            stringsElement: null,
            // typing speed
            typeSpeed: Math.floor(Math.random() * 10) + 1,
            // time before typing starts
            startDelay: 0,
            // backspacing speed
            backSpeed: Math.floor(Math.random() * 20) + 5,
            // time before backspacing
            backDelay: Math.floor(Math.random() * 10000) + 3000,
            // loop
            loop: false,
            // false = infinite
            loopCount: false,
            // show cursor
            showCursor: false,
            // character for cursor
            cursorChar: "|",
            // attribute to type (null == text)
            attr: null,
            // either html or text
            contentType: 'html',
            // call when done callback function
            callback: function() {

                first();

            },
            // starting callback function before each string
            preStringTyped: function(e) {


            },
            //callback for every typed string
            onStringTyped: function() {


            },
            // callback for reset
            resetCallback: function() {}
        });


    }


    function second(){

        var items = ["důvěřuj, ale prověřuj", "psané slovo na jednom místě"];
        var item = items[Math.floor(Math.random()*items.length)];

        $('.secondRotate').typed({
            strings: [item, ""], //Druhý parametr pro opačné slovo
            // Optionally use an HTML element to grab strings from (must wrap each string in a <p>)
            stringsElement: null,
            // typing speed
            typeSpeed: Math.floor(Math.random() * 10) + 1,
            // time before typing starts
            startDelay: 0,
            // backspacing speed
            backSpeed: Math.floor(Math.random() * 20) + 5,
            // time before backspacing
            backDelay: Math.floor(Math.random() * 10000) + 3000,
            // loop
            loop: false,
            // false = infinite
            loopCount: false,
            // show cursor
            showCursor: false,
            // character for cursor
            cursorChar: "|",
            // attribute to type (null == text)
            attr: null,
            // either html or text
            contentType: 'html',
            // call when done callback function
            callback: function() {

                second();

            },
            // starting callback function before each string
            preStringTyped: function(e) {


            },
            //callback for every typed string
            onStringTyped: function() {


            },
            // callback for reset
            resetCallback: function() {}
        });


    }

    $(function () {
        $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: ''
                    },
                    off: {
                        icon: ''
                    }
                };

            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                        .removeClass('btn-default')
                        .addClass('btn-' + color + ' active');
                }
                else {
                    $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });

    function test() {

        var fontSize = parseInt($('#List').css("font-size"));
        console.log(fontSize);

        if ( fontSize == "18"){

            $(".fontPlus").addClass(' noClick');

        }else if (fontSize == "14") {

            $(".fontMinus").addClass(' noClick');
        }

        else {

            $(".fontMinus").removeClass(' noClick');
            $(".fontPlus").removeClass(' noClick');


        }

    }

    $(".fontPlus").on("click", function (e) {
        e.preventDefault();
        if (e.handled !== true) { //Checking for the event whether it has occurred or not.
            e.handled = true; // Basically setting value that the current event has occurred.
            var fontSize = parseInt($('#List').css("font-size"));
            var fontSize2 = parseInt($('.itemTitle').css("font-size"));

            fontSize = fontSize + 1 + "px";
            fontSize2 = fontSize2 + 1 + "px";
            $('#List').css({'font-size':fontSize});
            $('.itemTitle').css({'font-size':fontSize2});

            test();
        }
    });

    $(".fontMinus").on("click", function (e) {
        e.preventDefault();
        if (e.handled !== true) { //Checking for the event whether it has occurred or not.
            e.handled = true; // Basically setting value that the current event has occurred.
            var fontSize = parseInt($('#List').css("font-size"));
            var fontSize2 = parseInt($('.itemTitle').css("font-size"));


            fontSize = fontSize - 1 + "px";
            fontSize2 = fontSize2 - 1 + "px";
            $('#List').css({'font-size':fontSize});
            $('.itemTitle').css({'font-size':fontSize2});

            test();
        }
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() == 0) {
            $('.arrowUp').addClass(' noClickArrow');
        } else if ($(window).scrollTop() > 1){
            $('.arrowUp').removeClass(' noClickArrow');
        }
    });

    $('.arrowUp').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

    if ($(window).width() <= 767) {

        $( "#List .listItem:even" ).css( "background-color", "#D3D3D3" );

    }

    $( window ).resize(function() {

        if ($(window).width() <= 767) {

            $( "#List .listItem:even" ).css( "background-color", "#D3D3D3" );

        }

    });

    $(document).ready(function(e){
        $(".searchid").keyup(function(e){
            filter();
        });
    });

    function filter(page) {
        var text = $(".searchid").val();

        var category = getCategories();

        $.ajax({
            type: "POST",
            url: "fetch.php",
            data: {text:text, page:page, category:category},
            dataType: "html",
            async: false,
            success: function(text){
                $('#List').html(text);
            }

        });

        $.ajax({
            type: "POST",
            url: "pageUpdate.php",
            data: {text:text, page:page, category:category},
            dataType: "html",
            async: false,
            success: function(text){
                $('.paginator').html(text);
            }

        });
    }

    $('.searchid').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {

            $(this).blur();

        }
    });

    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
    }

    $(document).on('click','#next',function(e){

        e.preventDefault();
        var url = ($(this).attr('href'));
        var page = getURLParameter(url, 'page');

        this.page = page;
        filter(page);

        return false;

    });

    $(document).on('click','button.btn',function(e){

        e.preventDefault();

        filter(page);

        return false;

    });

    function getCategories() {
        if ($('button.btn').hasClass("active")) {

            var checkedBoxes = [];

            if ($('button.btn.politics').hasClass("active")){
                checkedBoxes.push("politika");
            }

            if ($('button.btn.sport').hasClass("active")){
                checkedBoxes.push("sport");
            }

            if ($('button.btn.culture').hasClass("active")){
                checkedBoxes.push("kultura");
            }

            if ($('button.btn.economy').hasClass("active")){
                checkedBoxes.push("ekonomika");
            }

            if ($('button.btn.abroad').hasClass("active")){
                checkedBoxes.push("zahraničí");
            }

            if ($('button.btn.technology').hasClass("active")){
                checkedBoxes.push("technologie");
            }

            return checkedBoxes;

        }

        return null;

    }


    $(document).on('click','.radioBtn',function(e){

        if ($(this).hasClass("radioChecked")){



        }

        if ($(".radioBtn").hasClass("radioChecked")){

            $(".radioBtn").removeClass("radioChecked")

        }else {

            $(this).toggleClass("radioChecked");

        }



    });

});

