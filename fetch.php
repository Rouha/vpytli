<?php

require_once "app/admin/database.php";

$text = $_POST['text'];
if (isset($_POST['page'])) $page = $_POST['page'];
else $page = 1;

$category = $_POST['category'];

try {

    $subQuery = "";

    if (isset($category[0])){

        for ($i = 0; $i < sizeof($category); $i++) {
            if ($i == 0)
                $subQuery .= "AND category LIKE '%".$category[$i]."%'";
            else
                $subQuery .= " OR category LIKE '%$category[$i]%'";
        }

    }

    // Find out how many items are in the table
    $total = $databaseConnection->query("SELECT COUNT(*) FROM articles WHERE title LIKE '%$text%' OR description LIKE '%$text%' $subQuery")->fetchColumn();

    // How many items to list per page
    $limit = 20;

    // How many pages will there be
    $pages = ceil($total / $limit);

    // Calculate the offset for the query
    $offset = ($page - 1)  * $limit;

    // Some information to display to the user
    $start = $offset + 1;
    $end = min(($offset + $limit), $total);

    // The "back" link
    $prevlink = ($page > 1) ? '<a href="" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) .
        '" title="Previous page" id="prev">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';

    // The "forward" link
    $nextlink = ($page < $pages) ? '<a href="?page=' . ($page + 1) . '" title="Next page" id="next">&rsaquo;</a> <a href="?page=' . $pages .
        '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';

    // Prepare the paged query
    $stmt = $databaseConnection->prepare("SELECT gu_id, title, link, description, publish_date, category, server FROM articles WHERE title LIKE '%$text%' OR description LIKE '%$text%' $subQuery ORDER BY publish_date DESC LIMIT :limit OFFSET :offset");

    // Bind the query params
    $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
    $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
    $stmt->execute();

    var_dump($stmt);

    // Do we have any results?
    if ($stmt->rowCount() > 0) {
        // Define how we want to fetch the results
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $iterator = new IteratorIterator($stmt);

        // Display the results
        foreach ($iterator as $row) {

            echo '<a href="' . $row['link'] . '" target="_blank">';

            echo '<div class="listItem">';

            echo '<div class="color dva"><span>' . $row['category'] . '</span></div>';

            echo '<span class="itemTitle">' . $row['title'] . '</span>';

            echo '<p class="itemPerex">';

            echo $row['description'];

            echo '<div class="clearfix"></div>';

            echo '<span class="itemServer">' . $row['server'] . '</span>';

            echo '<span class="itemDate">' . $row['publish_date'] . '</span>';

            echo '</p>';

            echo '</div>';

            echo '</a>';

        }

        echo '<div class="clearfix pgnBottom"></div>';

    } else {
        echo '<p style="margin: 10px 0 0 15px;; font-size: 20px;">Ani prezident na člunu ...</p>';
    }

} catch (Exception $e) {
    echo '<p>', $e->getMessage(), '</p>';
}

